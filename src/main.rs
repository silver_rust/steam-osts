use steam_ost::{db_init, get_now, StoreDetails};
use sqlx::{Pool, Sqlite};
use std::env;
use dotenv::dotenv;
use tide::prelude::*;
use tide::{Request, Response};

#[derive(Clone)]
struct State {
    db: Pool<Sqlite>,
    config: Config,
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    let config = get_config();
    let db = db_init(&config.database).await?;

    let host_port = config.host_port.clone();
    
    tide::log::start();

    let state = State {
        db,
        config,
    };

    let mut app = tide::with_state(state);

    app.at("/steam_ost/:username").get(results_get);

    app.listen(host_port).await?;
    Ok(())
}

#[derive(Debug, Clone)]
struct Config {
    key: String,
    database: String,
    host_port: String,
}
fn get_config() -> Config {
    dotenv().ok();

    // reasonable defaults
    let mut config = Config {
        key: "".to_string(),
        database: "database.db".to_string(),
        host_port: "127.0.0.1:8063".to_string(),
    };
    
    if let Ok(x) = env::var("KEY") {
        config.key = x.trim().to_string();
    }
    if let Ok(x) = env::var("DATABASE") {
        config.database = x.trim().to_string();
    }
    if let Ok(x) = env::var("HOST_PORT") {
        config.host_port = x.trim().to_string();
    }

    config
}


async fn results_get(req: Request<State>) -> tide::Result {
    let pool = &req.state().db;
    let config = &req.state().config;

    let id = get_id(req.param("username").unwrap_or(""), &config.key).await;

    // need to sort out the error handling here
    if id.is_none() {
        return Ok(json!("not a valid id").into());
    }

    Ok(json!(vec![id]).into())
}

#[derive(Serialize, Deserialize, Debug)]
struct StoreResponseResponse {
    steamid: String,
    success: i32,
}
#[derive(Serialize, Deserialize, Debug)]
struct StoreResponse {
    response: StoreResponseResponse,
}
async fn get_id(input: &str, key: &str) -> Option<String> {
    let mut get_id = false;
    
    if input.len() != 17 || input.parse::<i64>().is_err() {
        get_id = true;
    } else {
        if let Ok(_) = input.parse::<i64>(){
            // only checking if its a proper number
            return Some(input.to_string());
        } else {
            get_id = true;
        }
    }
    
    if  get_id{
        let url = format!("https://api.steampowered.com/ISteamUser/ResolveVanityURL/v1/?key={}&vanityurl={}", key, input);
        if let Ok(result) = surf::get(url).recv_json::<StoreResponse>().await {
            if result.response.success == 1 {
                return Some(result.response.steamid)
            }
        }
    }
    
    // default bad value
    None
}

async fn get_games(id: &str, key: &str){
    let url = format!("https://api.steampowered.com/IPlayerService/GetOwnedGames/v1/?key={}&steamid={}", key, id);
    
}