use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
use sqlx::{Error, Pool, Sqlite};

use std::str::FromStr;
use std::time::{SystemTime, UNIX_EPOCH};
use tide::prelude::*;

pub async fn db_init(database: &str) -> Result<Pool<Sqlite>, Error> {
    let pool = SqlitePoolOptions::new()
        .max_connections(5)
        .connect_with(SqliteConnectOptions::from_str(&format!("sqlite://{}", database))?.create_if_missing(true))
        .await?;

    // https://store.steampowered.com/api/appdetails?appids=1258740
    sqlx::query(
        "CREATE TABLE IF NOT EXISTS store_details (
            id integer primary key,
            name text not null,
            item_type text not null,
            last_timestamp integer not null
         )",
    )
    .execute(&pool)
    .await?;

    // set up indexes?
    /*
    sqlx::query("CREATE INDEX IF NOT EXISTS index_estimate ON bus_results (valid_estimate)")
        .execute(&pool)
        .await?;
    */
    Ok(pool)
}

#[derive(Default, Debug, Serialize, Deserialize, sqlx::FromRow)]
#[serde(default)]
pub struct StoreDetails {
    id: i64,
    name: String,
    item_type: String,
    last_timestamp: i64
}

pub fn get_now() -> i64 {
    if let Ok(x) = SystemTime::now().duration_since(UNIX_EPOCH) {
        x.as_secs() as i64
    } else {
        0
    }
}
